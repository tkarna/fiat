# Copyright (C) 2008 Robert C. Kirby (Texas Tech University)
#
# This file is part of FIAT.
#
# FIAT is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FIAT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with FIAT. If not, see <http://www.gnu.org/licenses/>.

import numpy


def jrc(a, b, n, num_type):
    an = num_type((2*n+1+a+b)*(2*n+2+a+b)) \
        / num_type(2*(n+1)*(n+1+a+b))
    bn = num_type((a*a-b*b) * (2*n+1+a+b)) \
        / num_type(2*(n+1)*(2*n+a+b)*(n+1+a+b))
    cn = num_type((n+a)*(n+b)*(2*n+2+a+b)) \
        / num_type((n+1)*(n+1+a+b)*(2*n+a+b))
    return an, bn, cn


def lattice_iter(start, finish, depth):
    """Generator iterating over the depth-dimensional lattice of
    integers between start and (finish-1).  This works on simplices in
    1d, 2d, 3d, and beyond"""
    if depth == 0:
        return
    elif depth == 1:
        for ii in range(start, finish):
            yield [ii]
    else:
        for ii in range(start, finish):
            for jj in lattice_iter(start, finish-ii, depth - 1):
                yield [ii] + jj


def make_lattice(n, vs, numtype):
    hs = numpy.array([(vs[i] - vs[0]) / numtype(n)
                     for i in range(1, len(vs))]
                     )

    result = []

    m = len(hs)
    for indices in lattice_iter(0, n+1, m):
        res_cur = vs[0].copy()
        for i in range(len(indices)):
            res_cur += indices[i] * hs[m-i-1]
        result.append(res_cur)

    return numpy.array(result)


def make_triangle_lattice(n, numtype):
    vs = numpy.array([(numtype(-1), numtype(-1)),
                      (numtype(1), numtype(-1)),
                      (numtype(-1), numtype(1))])

    return make_lattice(n, vs, numtype)


def make_tetrahedron_lattice(n, numtype):
    vs = numpy.array([(numtype(-1), numtype(-1), numtype(-1)),
                      (numtype(1),  numtype(-1), numtype(-1)),
                      (numtype(-1), numtype(1),  numtype(-1)),
                      (numtype(-1), numtype(-1), numtype(1))
                      ])
    return make_lattice(n, vs, numtype)


def make_lattice_dim(D, n, numtype):
    if D == 2:
        return make_triangle_lattice(n, numtype)
    elif D == 3:
        return make_tetrahedron_lattice(n, numtype)


def tabulate_triangle(n, pts, numtype):
    return _tabulate_triangle_single(n, numpy.array(pts).T, numtype)


def _tabulate_triangle_single(n, pts, numtype):
    if len(pts) == 0:
        return numpy.array([], numtype)

    def idx(p, q):
        return (p+q)*(p+q+1)//2 + q

    results = (n+1)*(n+2)//2 * [None]

    results[0] = numtype(1) \
        + pts[0] - pts[0] \
        + pts[1] - pts[1]

    if n == 0:
        return results

    x = pts[0]
    y = pts[1]

    one = numtype(1)
    two = numtype(2)
    three = numtype(3)

    # foo = one + two*x + y

    f1 = (one+two*x+y)/two
    f2 = (one - y) / two
    f3 = f2**2

    results[idx(1, 0), :] = f1

    for p in range(1, n):
        a = (two * p + 1) / (1 + p)
        # b = p / (p + one)
        results[idx(p+1, 0)] = a * f1 * results[idx(p, 0), :] \
            - p/(one+p) * f3 * results[idx(p-1, 0), :]

    for p in range(n):
        results[idx(p, 1)] = (one + two*p+(three+two*p)*y) / two \
            * results[idx(p, 0)]

    for p in range(n-1):
        for q in range(1, n-p):
            (a1, a2, a3) = jrc(2*p+1, 0, q, numtype)
            results[idx(p, q+1)] = \
                (a1 * y + a2) * results[idx(p, q)] \
                - a3 * results[idx(p, q-1)]

    return results


def tabulate_tetrahedron(n, pts, numtype):
    return _tabulate_tetrahedron_single(n, numpy.array(pts).T, numtype)


def _tabulate_tetrahedron_single(n, pts, numtype):
    def idx(p, q, r):
        return (p+q+r)*(p+q+r+1)*(p+q+r+2)//6 + (q+r)*(q+r+1)//2 + r

    results = (n+1)*(n+2)*(n+3)//6 * [None]
    results[0] = 1.0 \
        + pts[0] - pts[0] \
        + pts[1] - pts[1] \
        + pts[2] - pts[2]

    if n == 0:
        return results

    x = pts[0]
    y = pts[1]
    z = pts[2]

    one = numtype(1)
    two = numtype(2)
    three = numtype(3)

    factor1 = (two + two*x + y + z) / two
    factor2 = ((y+z)/two)**2
    factor3 = (one + two * y + z) / two
    factor4 = (1 - z) / two
    factor5 = factor4 ** 2

    results[idx(1, 0, 0)] = factor1
    for p in range(1, n):
        a1 = (two * p + one) / (p + one)
        a2 = p / (p + one)
        results[idx(p+1, 0, 0)] = a1 * factor1 * results[idx(p, 0, 0)] \
            - a2 * factor2 * results[idx(p-1, 0, 0)]

    for p in range(0, n):
        results[idx(p, 1, 0)] = results[idx(p, 0, 0)] \
            * (p * (one + y) + (two + three * y + z) / two)

    for p in range(0, n-1):
        for q in range(1, n-p):
            (aq, bq, cq) = jrc(2*p+1, 0, q, numtype)
            qmcoeff = aq * factor3 + bq * factor4
            qm1coeff = cq * factor5
            results[idx(p, q+1, 0)] = qmcoeff * results[idx(p, q, 0)] \
                - qm1coeff * results[idx(p, q-1, 0)]

    for p in range(n):
        for q in range(n-p):
            results[idx(p, q, 1)] = results[idx(p, q, 0)] \
                * (one + p + q + (two + q + p) * z)

    for p in range(n-1):
        for q in range(0, n-p-1):
            for r in range(1, n-p-q):
                ar, br, cr = jrc(2*p+2*q+2, 0, r, numtype)
                results[idx(p, q, r+1)] = \
                    (ar * z + br) * results[idx(p, q, r)] \
                    - cr * results[idx(p, q, r-1)]

    return results


def tabulate_tetrahedron_derivatives(n, pts, numtype):
    D = 3
    order = 1
    return tabulate_jet(D, n, pts, order, numtype)


def tabulate(D, n, pts, numtype):
    return _tabulate_single(D, n, numpy.array(pts).T, numtype)


def _tabulate_single(D, n, pts, numtype):
    if D == 2:
        return _tabulate_triangle_single(n, pts, numtype)
    elif D == 3:
        return _tabulate_tetrahedron_single(n, pts, numtype)


def tabulate_jet(D, n, pts, order, numtype):
    from .expansions import _tabulate_dpts

    # Wrap the tabulator to allow for nondefault numtypes
    def tabulator_wrap(n, X):
        return _tabulate_single(D, n, X, numtype)

    data1 = _tabulate_dpts(tabulator_wrap, D, n, order, pts)
    # Put data in the required data structure, i.e.,
    # k-tuples which contain the value, and the k-1 derivatives
    # (gradient, Hessian, ...)
    m = data1[0].shape[0]
    n = data1[0].shape[1]
    data2 = [[tuple([data1[r][i][j] for r in range(order+1)])
              for j in range(n)]
             for i in range(m)]
    return data2


if __name__ == "__main__":
    import gmpy

    latticeK = 2
    D = 3

    pts = make_tetrahedron_lattice(latticeK, gmpy.mpq)

    vals = tabulate_tetrahedron_derivatives(D, pts, gmpy.mpq)

    print(vals)
